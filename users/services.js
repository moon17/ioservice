const {
    query
} = require('../data');

const getUsers = async () => {
    console.info(`Getting all users ...`);

    const users = await query("SELECT id, username, role_id FROM users");

    return users;
};

const getUserById = async (id) => {
    console.info(`Getting user ${id} ...`);

    const users = await query("SELECT username, role_id FROM users WHERE id = $1", [id]);

    return users[0];
};

const addUser = async (username, password) => {
    console.info(`Adding user ${username}, password ${password} ...`);

    const users = await query("INSERT INTO users (username, password) VALUES ($1, $2) RETURNING id, username", [username, password]);

    return users[0].id;
}; 

const getUserByUsername = async (username) => {
    console.info(`Getting users with username ${username} ...`);

    const users = await query("SELECT username, role_id FROM users WHERE username = $1", [password]);

    return users;
};

const updateUserRole = async (id, role_id) => {
    console.info(`Updating user's ${id} role`);

    const user = await query(`UPDATE users SET role_id = $2 WHERE id = $1`, [id, role_id]);

    return user;
}

const getByUsernameWithRole = async (username) => {
    console.info(`Getting user with username ${username}`);
    
    const users = await query(`SELECT u.id, u.password, 
                                u.username, r.value as role,
                                r.id as role_id FROM users u 
                                JOIN roles r ON r.id = u.role_id
                                WHERE u.username = $1`, [username]);
    return users[0];
};


module.exports = {
    getUsers,
    getUserById,
    addUser,
    getUserByUsername,
    updateUserRole,
    getByUsernameWithRole
}
