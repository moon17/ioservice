const Router = require('express').Router();

const GamesController = require('./games/controllers.js');
const RolesController = require('./roles/controllers.js');
const UsersController = require('./users/controllers.js');

Router.use('/games', GamesController);
Router.use('/roles', RolesController);
Router.use('/users', UsersController);

module.exports = Router;
