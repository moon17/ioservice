const jwt = require('jsonwebtoken');

const ServerError = require('../../Models/ServerError.js');

const options = {
    issuer: "issuer",
    subject: "subj",
    audience: "client"
};

const generateToken = async (payload) => {

    try {
        const token = jwt.sign({
            userId: payload.UserId,
            userRole: payload.UserRole
        }, "secretkey", options);
        return token;
    } catch (err) {
        console.trace(err);
        throw new ServerError("The token can not be signed", 500);
    }
};

const verifyAndDecodeData = async (token) => {
    try {
        const decoded = jwt.verify(token, "secretkey", options);
        return decoded;
    } catch (err) {
        console.trace(err);
        throw new ServerError("The token can not be decrypted", 401);
    }
};

module.exports = {
    generateToken,
    verifyAndDecodeData
};
