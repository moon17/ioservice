const UsersRepository = require('../users/services.js');
const AuthenticatedUserDto = require('../DTOs/AuthenticatedUserDto.js');
const RegisteredUserDto = require('../DTOs/RegisteredUserDto.js');
const JwtPayloadDto = require('../DTOs/JwtPayloadDto.js');

const { hashPassword, comparePlainTextToHashedPassword } = require('../Security/Password')
const { generateToken } = require('../Security/JWT');
const ServerError = require('../Models/ServerError.js');

const authenticate = async (username, plainTextPassword) => {

    console.info(`Authenticates user with username ${username}`);

    const user = await UsersRepository.getByUsernameWithRole(username);
    
    if (!user) {
        throw new ServerError(`User ${username} does not exist!`, 404);
    }

    var isOk = await comparePlainTextToHashedPassword(plainTextPassword, user.password);

    if (!isOk) {
        throw new ServerError('Wrong password!', 403);
    }

    const token = await generateToken(new JwtPayloadDto(user.id, user.role));

    return new AuthenticatedUserDto(token, user.username, user.role);
};

const register = async (username, plainTextPassword) => {

    const hashedPassword = await hashPassword(plainTextPassword);

    const user = await UsersRepository.addUser(username, hashedPassword);

    return new RegisteredUserDto(user.id, username);
};

module.exports = {
    authenticate,
    register
}
