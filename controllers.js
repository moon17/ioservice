const Router = require('express').Router();

const {
    getUserById,
    getUsers,
    addUser,
    getUserByUsername,
    updateUserRole,
    getByUsernameWithRole
} = require('./services.js');

const UsersManager = require('./../../authenticationservice/Managers/UsersManager.js');

const {
    UserBody,
    UserRegisterResponse,
    UserLoginResponse
} = require ('./../../authenticationservice/Models/User.js');
const ResponseFilter = require('./../../authenticationservice/Filters/ResponseFilter.js');

Router.get('/', async (req, res) => {
    
    const users = await getUsers();

    res.json(users);
});

Router.get('/username=:username', async (req, res) => {

    const {
        username
    } = req.params;

    const user = await getUserByUsername(username);

    res.json(user);
});

Router.get('/id=:id', async (req, res) => {

    const {
        id
    } = req.params;

    const user = await getUserById(id);

    res.json(user);
});

Router.post('/', async (req, res) => {

    const {
        username,
        password
    } = req.body;

    const id = await addUser(username, password);

    res.json(id);
});

Router.put('/:id/role/:role_id', async (req, res) => {
    let {
        id,
        role_id
    } = req.params;

    id = parseInt(id);
    role_id = parseInt(role_id);

    const user = await updateUserRole(id, role_id);
    res.json(user);
});

Router.get('/:username', async (req, res) => {

    const {
        username
    } = req.params;

    const user = await getByUsernameWithRole(username);

    res.json(user);
});

Router.post('/register', async (req, res) => {

    const userBody = new UserBody(req.body);
    const user = await UsersManager.register(userBody.Username, userBody.Password);

    ResponseFilter.setResponseDetails(res, 201, new UserRegisterResponse(user));
});

Router.post('/login', async (req, res) => {

    const userBody = new UserBody(req.body);
    const userDto = await UsersManager.authenticate(userBody.Username, userBody.Password);
    const user = new UserLoginResponse(userDto.Token, userDto.Role);

    ResponseFilter.setResponseDetails(res, 200, user);
});

module.exports = Router;
