const {
    query
} = require('../data');

const getGames = async () => {
    console.info(`Getting all games ...`);

    const games = await query("SELECT id, name, genre, description FROM games_table");

    return games;
};

const getGameById = async (id) => {
    console.info(`Getting game ${id} ...`);

    const games = await query("SELECT name, genre, description FROM games_table WHERE id = $1", [id]);

    return games[0];
};

const addGame = async (name, genre, description) => {
    console.info(`Adding game ${name}, genre ${genre} with description ${description}...`);

    const games = await query("INSERT INTO games_table (name, genre, description) VALUES ($1, $2, $3) RETURNING id", [name, genre, description]);

    return games[0].id;
}; 

const getGameByGenre = async (genre) => {
    console.info(`Getting games with genre ${genre} ...`);

    const games = await query("SELECT name, genre, description FROM games_table WHERE genre = $1", [genre]);

    return games;
};


module.exports = {
    getGames,
    getGameById,
    addGame,
    getGameByGenre
}
