const Router = require('express').Router();

const {
    getGameById,
    getGames,
    addGame,
    getGameByGenre
} = require('./services.js');

Router.get('/', async (req, res) => {
    
    const games = await getGames();

    res.json(games);
});

Router.get('/genre=:genre', async (req, res) => {

    const {
        genre
    } = req.params;

    const game = await getGameByGenre(genre);

    res.json(game);
});

Router.get('/id=:id', async (req, res) => {

    const {
        id
    } = req.params;

    const game = await getGameById(id);

    res.json(game);
});

Router.post('/', async (req, res) => {

    const {
        name,
        genre,
        description
    } = req.body;

    const id = await addGame(name, genre, description);

    res.json(id);
});

module.exports = Router;
