const {
    query
} = require('../data');

const getRoles = async () => {
    console.info(`Getting all roles ...`);

    const roles = await query("SELECT id, value FROM roles");

    return roles;
};

const getRoleById = async (id) => {
    console.info(`Getting role ${id} ...`);

    const role = await query("SELECT value FROM roles WHERE id = $1", [id]);

    return role[0];
};

const addRole = async (value) => {
    console.info(`Adding role ${value} ...`);

    const role = await query("INSERT INTO roles (value) VALUES ($1) RETURNING id", [value]);

    return role[0].id;
}; 

module.exports = {
    getRoles,
    getRoleById,
    addRole
}
