const Router = require('express').Router();

const {
    getRoles,
    getRoleById,
    addRole
} = require('./services.js');

Router.get('/', async (req, res) => {
    
    const roles = await getRoles();

    res.json(roles);
});

Router.get('/:id', async (req, res) => {

    const {
        id
    } = req.params;

    const role = await getRoleById(id);

    res.json(role);
});

Router.post('/', async (req, res) => {

    const {
        value
    } = req.body;

    const id = await addRole(value);

    res.json(id);
});


module.exports = Router;
